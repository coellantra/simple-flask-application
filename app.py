"""A simple Hello world application"""
from flask import Flask

APP = Flask(__name__)
"""Applications index route function"""


@APP.route('/')
def index():
  return 'Hello, World!'

if __name__ == '__main__':
  APP.run(host='127.0.0.1', port=5000, debug=True)
